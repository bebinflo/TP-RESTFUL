// Création de la carte
var map = new ol.Map({
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM()
    })
  ],
  target: 'map',
  view: new ol.View({
    projection: 'EPSG:4326',
    center: [1.0687, 49.387],
    zoom: 18
  })
});

// ajouter du gestionnaire de clic
map.on("click", function(e) {
	// on récupère la position
  var lonlat = e.coordinate;
  var lat = lonlat[1].toFixed(4);
  var lng = lonlat[0].toFixed(4);
  // on l'affiche
  $("#mouse-position-click").html("You clicked near " + lonlat[1].toFixed(4) + " N, " + lonlat[0].toFixed(4) + " E");
  // on requête le web service geoname pour retrouver la ville/ bâtiment qui correspond aux coordonnées géographiques
 $.get("http://api.geonames.org/findNearbyJSON?lat="+lat+"&lng="+lng+"&username=apple",function(data){
    // on marshall / sérialise le XML information en chaînes de caractères
    //var xml = new XMLSerializer().serializeToString(data);
    var info = data.geonames[0].name.split(" ");
    var request = "https://api.wolframalpha.com/v2/query?input="+info[0];
    for (let index = 1; index < info.length; index++) {
      request += "+" + info[index];
    }
    request += "&format=image,plaintext&output=XML&appid=DEMO";
    $.get(request, function(data2){
    	var xml = new XMLSerializer().serializeToString(data2);
      $("#info").text(xml);
    })
    var json = JSON.stringify(data);
    // on l'affiche
    $("#info").text(json + info);
  });
  
  // TODO: appelez la version JSON de ce même web service
  
});


