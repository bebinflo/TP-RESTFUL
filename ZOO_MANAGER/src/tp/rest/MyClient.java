package tp.rest;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class MyClient {
	private Service service;
	private JAXBContext jc;

	private static final QName qname = new QName("", "");
	private static final String url = "http://127.0.0.1:8084";

	public MyClient() {
		try {
			jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
		} catch (JAXBException je) {
			System.out.println("Cannot create JAXBContext " + je);
		}
	}
	public void get_animal(String uuid) throws JAXBException{
		service = Service.create(qname);
		service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + uuid);
		Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
		Map<String, Object> requestContext = dispatcher.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
		Source result = dispatcher.invoke(null);
		printSource(result);
	}

	public void add_animal(Animal animal) throws JAXBException {
		service = Service.create(qname);
		service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
		Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
		Map<String, Object> requestContext = dispatcher.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	public void delete() throws JAXBException {
		Animal animal = new Animal("", "", "", UUID.randomUUID());
		service = Service.create(qname);
		service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
		Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
		Map<String, Object> requestContext = dispatcher.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	public void printSource(Source s) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.transform(s, new StreamResult(System.out));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String args[]) throws Exception {
		MyClient client = new MyClient();
		// Scenario
		System.out.println("Affichez l'ensemble des animaux");
		client.get_animal("");
		
		System.out.println("\nSupprimez touts les animaux");
		client.delete();
		
		System.out.println("\nAffichez l'ensemble des animaux");
		client.get_animal("");
		
		System.out.println("\nAjoutez un Panda à Rouen (Latitude : 49.443889 ; Longitude : 1.103333)");
		client.add_animal(new Animal("Pandi","Rouen","Panda",UUID.randomUUID()));
		
		System.out.println("\nAjoutez un Hocco unicorne à Paris (Latitude : 48.856578 ; Longitude : 2.351828)");
		client.add_animal(new Animal("Hocco unicorne","paris","Hocco unicorne",UUID.randomUUID()));
		
		System.out.println("\nAffichez l'ensemble des animaux");
		client.get_animal("");
	}
}
