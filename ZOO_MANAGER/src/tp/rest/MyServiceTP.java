package tp.rest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.ListAnimals;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {

	public final static String url = "http://127.0.0.1:8084/";

	public static void main(String args[]) {
		Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());

		e.publish(url);
		System.out.println("Service started, listening on " + url);
		// pour arrêter : e.stop();
	}

	private JAXBContext jc;

	@javax.annotation.Resource(type = Object.class)
	protected WebServiceContext wsContext;

	private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

	public MyServiceTP() {
		try {
			jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
		} catch (JAXBException je) {
			System.out.println("Exception " + je);
			throw new WebServiceException("Cannot create JAXBContext", je);
		}

		// Fill our center with some animals
		Cage usa = new Cage(
				"usa",
				new Position(49.305d, 1.2157357d),
				25,
				new LinkedList<>(Arrays.asList(
						new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
						new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
						))
				);

		Cage amazon = new Cage(
				"amazon",
				new Position(49.305142d, 1.2154067d),
				15,
				new LinkedList<>(Arrays.asList(
						new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
						new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
						new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
						new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
						))
				);

		center.getCages().addAll(Arrays.asList(usa, amazon));
	}

	public Source invoke(Source source) {
		MessageContext mc = wsContext.getMessageContext();
		String path = (String) mc.get(MessageContext.PATH_INFO);
		String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

		// determine the targeted ressource of the call
		try {
			// no target, throw a 404 exception.
			if (path == null) {
				throw new HTTPException(404);
			}
			// "/animals" target - Redirect to the method in charge of managing this sort of call.
			else if (path.startsWith("animals")) {
				String[] path_parts = path.split("/");
				switch (path_parts.length){
				case 1 :
					return this.animalsCrud(method, source);
				case 2 :
					return this.animalCrud(method, source, path_parts[1]);
				default:
					throw new HTTPException(404);
				}
			}
			else if (path.startsWith("find/")) {
				throw new HTTPException(503);
			}
			else if ("coffee".equals(path)) {
				throw new HTTPException(418);
			}
			else {
				throw new HTTPException(404);
			}
		} catch (JAXBException e) {
			throw new HTTPException(500);
		}
	}

	/**
	 * Method bound to calls on /find/byName/{name}
	 */
	private Source findAnimalByName(String method, Source source, String animal_name) throws JAXBException {
		try {
			return new JAXBSource(this.jc, center.findAnimalByName(animal_name));
		} catch (AnimalNotFoundException e) {
			throw new HTTPException(404);
		}
	}

	/**
	 * Method bound to calls on /animals/{something}
	 */
	private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
		if("GET".equals(method)){
			try {
				return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
			} catch (AnimalNotFoundException e) {
				throw new HTTPException(404);
			}
		}
		else if("POST".equals(method)){
			// On cherche l'animal
			try {
				// Si on le trouve pas besoin de l'ajouter
				this.center.findAnimalById(UUID.fromString(animal_id));
				//throw new AnimalAlreadyExistExecption();
			} catch (AnimalNotFoundException e) {
				// Si il n'existe pas on l'ajoute
				Animal animal;
				animal = unmarshalAnimal(source);
				animal.setId(UUID.fromString(animal_id));
				this.center.getCages()
				.stream()
				.filter(cage -> cage.getName().equals(animal.getCage()))
				.findFirst()
				.orElseThrow(() -> new HTTPException(404))
				.getResidents()
				.add(animal);
			}
			return new JAXBSource(this.jc, this.center);
		}
		else if("DELETE".equals(method)) {
			// On cherche dans toutes les cages les animaux d'id 'animal_id'
			// puis on les supprimes
			this.center.getCages()
			.stream()
			.forEach(cage -> cage.getResidents()
					.removeIf(animal -> animal.getId()
							.equals(UUID.fromString(animal_id))
							)
					);
			return new JAXBSource(this.jc, this.center);
		}
		else if("PUT ".equals(method)) {
			try {
				Animal truc;
				truc = this.center.findAnimalById(UUID.fromString(animal_id));
				Animal animal;
				animal = unmarshalAnimal(source);
				truc.setCage(animal.getCage());
				truc.setName(animal.getName());
				truc.setSpecies(animal.getSpecies());
			} catch (AnimalNotFoundException e) {
				throw new HTTPException(404);
			}
			return new JAXBSource(this.jc, this.center);
		}
		else{
			throw new HTTPException(405);
		}
	}

	/**
	 * Method bound to calls on /animals
	 */
	private Source animalsCrud(String method, Source source) throws JAXBException {
		if("GET".equals(method)){
			return new JAXBSource(this.jc, this.center);
		}
		else if("POST".equals(method)){
			Animal animal = unmarshalAnimal(source);
			
			if (animal == null) {
				System.out.println("POST NULL ANIMAL");
				return new JAXBSource(this.jc, this.center);
			}
			
			
			// Si la cage n'existe pas
			if(!this.center.getCages()
					.stream()
					.filter(cage -> cage.getName().equals(animal.getCage()))
					.findFirst()
					.isPresent()) {
				Cage cage = new Cage();
				cage.setName(animal.getCage());
				cage.setResidents(new LinkedList<>(Arrays.asList(animal)));
				this.center.getCages().add(cage);
				return new JAXBSource(this.jc, this.center);
			}
			
			// si la cage existe
			this.center.getCages()
			.stream()
			.filter(cage -> cage.getName().equals(animal.getCage()))
			.findFirst()
			.orElseThrow(() -> new HTTPException(404))
			.getResidents()
			.add(animal);
			return new JAXBSource(this.jc, this.center);
		}
		else if ("PUT".equals(method)) {
			ListAnimals list = unmarshalListAnimal(source) ;
			for (Animal a : list.getList()) {
				try {
					Animal truc;
					truc = this.center.findAnimalById(a.getId());
					truc.setCage(a.getCage());
					truc.setName(a.getName());
					truc.setSpecies(a.getSpecies());
				} catch (AnimalNotFoundException e) {
					// RIEN
				}
			}
			return new JAXBSource(this.jc, this.center);
		}
		else if("DELETE".equals(method)) {
			this.center.getCages()
			.stream()
			.forEach(cage -> cage.getResidents().clear() );
			return new JAXBSource(this.jc, this.center);
		}
		else{
			throw new HTTPException(405);
		}
	}

	private Animal unmarshalAnimal(Source source) throws JAXBException {
		return (Animal) this.jc.createUnmarshaller().unmarshal(source);
	}

	private ListAnimals unmarshalListAnimal(Source source) throws JAXBException {
		return (ListAnimals) this.jc.createUnmarshaller().unmarshal(source);
	}

}
