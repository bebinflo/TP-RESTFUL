package tp.model;

import java.util.*;

import javax.xml.bind.annotation.XmlElement;

public class ListAnimals {
	
	@XmlElement( name="animal" )
	private List<Animal> list;


	public ListAnimals() {
		list = new LinkedList<Animal>();
	}

	public ListAnimals(List<Animal> l) {
		list = l;
	}

	public List<Animal> getList() {
		return list;
	}

	public void add(Animal e) {
		list.add(e);
	}
}
